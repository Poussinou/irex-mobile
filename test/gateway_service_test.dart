import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;
import 'package:space_irex_mobile/main.dart';
import 'package:space_irex_mobile/model/User.dart';
import 'package:space_irex_mobile/services/gateway_service.dart';

// Creer un mock pour client http.Client

class MockClient extends Mock implements http.Client {}

void main() {
  test('Gateway call test', () async {
    // Creation de l'instance de service de la gateway en utilisant le mock client
    final gatewayService = GatewayService.withClient(
        MockClient(), 'https://votre-gateway-url.com');

    // Comporte attendu après une requête
    when(gatewayService.client
        .post(Uri.parse('https://votre-gateway-url.com'), headers: anyNamed('headers'), body: anyNamed('body')))
        .thenAnswer((_) async => http.Response('{"status": "success"}', 201));

    // Creer un user fictif
    final user = User(
        "1",
        'John',
        'Doe',
        "john.doe@example.com",
        '123456789');

    // Appeler la fonction de la gateway
    await gatewayService.register(user);
    // Vérifier que la requête a bien été envoyée
    verify(gatewayService.client.post(
      Uri.parse('https://votre-gateway-url.com/votre-endpoint'),
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      body:
      '{"noms":"John","prenoms":"Doe","email":"john.doe@example.com","tel":"123456789"}',
    ));
  });
}
