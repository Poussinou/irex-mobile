import 'package:encrypt/encrypt.dart';


final key = Key.fromSecureRandom(32);
final iv = IV.fromSecureRandom(16);
final encrypter = Encrypter(AES(key));


String encrypt(String text){
  final encrypted = encrypter.encrypt(text, iv: iv);
  return encrypted.base64;
}

String decrypt(String text){
  final decrypted = encrypter.decrypt(Encrypted.fromBase64(text), iv: iv);
  return decrypted;
}