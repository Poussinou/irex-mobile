import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:space_irex_mobile/model/User.dart';
import 'package:space_irex_mobile/pages/Login.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:space_irex_mobile/widget/Bouton.dart';
import 'package:space_irex_mobile/pages/HomeStart.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:file_picker/file_picker.dart';

import '../services/gateway_service.dart';
//import 'package:filesystem_picker/filesystem_picker.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key, required this.title});

  final String title;

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {

  var logger = Logger();

  final TextEditingController noms = TextEditingController();
  final TextEditingController prenoms = TextEditingController();
  final TextEditingController email = TextEditingController();
  final TextEditingController tel = TextEditingController();

  String pathFileCV = "path";



  String radioButtonItem = 'Masculin';
  int id = 1;


  void s_inscrire(){

    if(_formkey.currentState!.validate()){

      User utilisateur= new User.empty();

      utilisateur.noms = noms.text;
      utilisateur.prenoms = prenoms.text;
      utilisateur.email = email.text;
      utilisateur.tel = tel.text;

      logger.log(Level.info, "userName : ${utilisateur.noms} ${utilisateur.prenoms} et password : ${utilisateur.email} )");

      var keyRegister = GatewayService.withDefaultUrl(http.Client()).register(utilisateur);


      keyRegister?.then((value) => setState(() {

        if (value?.statusCode == 201) {

          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
                return LoginPage(title: "first log");
              }));

          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.green, content: Text("Utilisateur ajouté avec success",textAlign: TextAlign.center, style: TextStyle(color: Colors.white),)));
        } else if(value?.statusCode == 409){
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.red, content: Text("L'utilisateur existe déjà",textAlign: TextAlign.center, style: TextStyle(color: Colors.white),)));
        }

      }));

    }

  }



  final _formkey = GlobalKey<FormState>();
  DateTime? currentBackPressTime;

  @override
  Widget build(BuildContext context) {

    return WillPopScope(
        child: Form(
            key: _formkey,
            child: Scaffold(

              body: SingleChildScrollView(
                  child: Stack(
                    clipBehavior: Clip.none,
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.transparent,
                        child: Ink.image(
                          image: const AssetImage("images/acc2.jpg"),
                          fit: BoxFit.cover,
                          width: MediaQuery.of(context).size.height,
                          height: MediaQuery.of(context).size.width,
                          child: InkWell(
                            onTap: () {
                            },
                          ),
                        ),
                      ),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: MediaQuery.of(context).size.height/2.7,),
                          Container(
                              padding: const EdgeInsets.only(bottom: 5, top: 5),
                              height: MediaQuery.of(context).size.height/1.588,
                              width: MediaQuery.of(context).size.width,
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(topRight: Radius.circular(50),topLeft: Radius.circular(50) )),
                              child: Column(
                                children: [
                                  Container(
                                    height: MediaQuery.of(context).size.height/2.15,
                                    child: SingleChildScrollView(

                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                        children: [

                                        SizedBox(height: 15,),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          height: 65,
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width / 1.25,
                                          decoration: BoxDecoration(
                                            color: Colors.orange.shade50,
                                            boxShadow: const [
                                              BoxShadow(
                                                  offset: Offset(0, 0),
                                                  blurRadius: 0,
                                                  color: Colors.transparent)
                                            ],
                                          ),
                                          child:  Center(
                                            child: TextFormField(
                                              controller: noms,
                                              validator: (value) {
                                                if (value == null || value.isEmpty) {
                                                  return AppLocalizations.of(context)!.lastnameRequire;
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                border: UnderlineInputBorder(),
                                                hintStyle: TextStyle(fontSize: 14),
                                                hintText: AppLocalizations.of(context)!.name,
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 18,),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          height: 65,
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width / 1.25,
                                          decoration: BoxDecoration(
                                            color: Colors.orange.shade50,
                                            boxShadow: const [
                                              BoxShadow(
                                                  offset: Offset(0, 0),
                                                  blurRadius: 0,
                                                  color: Colors.transparent)
                                            ],
                                          ),
                                          child:  Center(
                                            child: TextFormField(
                                              controller: prenoms,
                                              validator: (value) {
                                                if (value == null || value.isEmpty) {
                                                  return AppLocalizations.of(context)!.firstnameRequire;
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                border: UnderlineInputBorder(),
                                                hintStyle: TextStyle(fontSize: 14),
                                                hintText: AppLocalizations.of(context)!.firstname,
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 18,),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          height: 65,
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width / 1.25,
                                          decoration: BoxDecoration(
                                            color: Colors.orange.shade50,
                                            boxShadow: const [
                                              BoxShadow(
                                                  offset: Offset(0, 0),
                                                  blurRadius: 0,
                                                  color: Colors.transparent)
                                            ],
                                          ),
                                          child:  Center(
                                            child: TextFormField(
                                              controller: email,
                                              validator: (value) {
                                                if (value == null || value.isEmpty) {
                                                  return AppLocalizations.of(context)!.emailRequire;
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                border: UnderlineInputBorder(),
                                                hintStyle: TextStyle(fontSize: 14),
                                                hintText: AppLocalizations.of(context)!.email,
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(height: 18,),
                                        Container(
                                          padding: const EdgeInsets.all(5),
                                          height: 65,
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width / 1.25,
                                          decoration: BoxDecoration(
                                            color: Colors.orange.shade50,
                                            boxShadow: const [
                                              BoxShadow(
                                                  offset: Offset(0, 0),
                                                  blurRadius: 0,
                                                  color: Colors.transparent)
                                            ],
                                          ),
                                          child:  Center(
                                            child: TextFormField(
                                              controller: tel,
                                              validator: (value) {
                                                if (value == null || value.isEmpty) {
                                                  return AppLocalizations.of(context)!.phoneRequire;
                                                }
                                                return null;
                                              },
                                              decoration: InputDecoration(
                                                border: UnderlineInputBorder(),
                                                hintStyle: TextStyle(fontSize: 14),
                                                hintText: AppLocalizations.of(context)!.phone,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),),

                                  buildButton(AppLocalizations.of(context)!.registerButton,Colors.white,CupertinoColors.systemBlue,s_inscrire),

                                  SizedBox(height: 15,),

                                  RichText(
                                    text: TextSpan(
                                        text: AppLocalizations.of(context)!.ifAccount,
                                        style: const TextStyle(color: Colors.black, fontSize: 14),
                                        children: [
                                          TextSpan(
                                            text: AppLocalizations.of(context)!.loginButton,
                                            style: TextStyle(color: Colors.orange, fontWeight: FontWeight.bold, fontSize: 20),
                                            recognizer: TapGestureRecognizer()
                                              ..onTap = () {
                                                Navigator.push(context,
                                                    MaterialPageRoute(builder: (BuildContext context) {
                                                      return LoginPage(title: "title");
                                                    }));
                                              },
                                          ),
                                        ]),
                                  ),
                                  SizedBox(height: 5,),
                                ],
                              )


                          ),
                        ],
                      ),
                    ],
                  )
              ),

            )),
        onWillPop: (){
          DateTime now = DateTime.now();
          if(currentBackPressTime == null || now.difference(currentBackPressTime!)>Duration(seconds: 2)){
            currentBackPressTime=now;
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.transparent, content: Text("Tap again to Exit",textAlign: TextAlign.center, style: TextStyle(color: Colors.black),)));
            return Future.value(false);
          }else{
            SystemNavigator.pop();
            return Future.value(false);}
        });


  }


  Future pickFile() async {

    FilePickerResult? result = await FilePicker.platform.pickFiles();

    if(result != null){
      File file = File(result.files.single.path!);
      print(file);
      ScaffoldMessenger.of(context).showSnackBar( SnackBar(backgroundColor: Colors.green, content: Text("file $file added"),));

      setState(() {
        pathFileCV = file.path;
      });
      print(pathFileCV);


    }else {
      print("Le fichier n'a pas chargé ");
    }
  }



}
