import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:space_irex_mobile/keycloak/env.dart';
import 'package:space_irex_mobile/model/User.dart';
import 'package:space_irex_mobile/widget/Bouton.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:space_irex_mobile/pages/HomeStart.dart';
import 'package:space_irex_mobile/pages/Register.dart';

import 'package:http/http.dart' as http;

import '../encrypt/crypt.dart';
import '../services/gateway_service.dart';



class LoginPage extends StatefulWidget {
  const LoginPage({super.key, required this.title});

  final String title;

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  var logger = Logger();

  final TextEditingController userName = TextEditingController();
  final TextEditingController psw = TextEditingController();

  User utilisateur = new User.empty();

  void connection() {
    if (_formkey.currentState!.validate()) {

      logger.log(Level.info, "login userName : ${encrypt(userName.text)} et password : ${encrypt(psw.text)} ");

      var keyConnect = GatewayService.withDefaultUrl(http.Client()).login(encrypt(userName.text), encrypt(psw.text));

      keyConnect?.then((value) => setState(() {

        if(value?.statusCode == 200){

          utilisateur.token=value!.message;

          Navigator.push(context,
              MaterialPageRoute(builder: (BuildContext context) {
                return HomeStartPage(user: utilisateur);
              }));

        }else if(value?.statusCode == 401){
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.red, content: Text("Données invalident",textAlign: TextAlign.center, style: TextStyle(color: Colors.white),)));

        }else if(value?.statusCode == 400){
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.red, content: Text("Données invalident",textAlign: TextAlign.center, style: TextStyle(color: Colors.white),)));

        }else{
          ScaffoldMessenger.of(context).showSnackBar(const SnackBar(backgroundColor: Colors.red, content: Text("Données invalident",textAlign: TextAlign.center, style: TextStyle(color: Colors.white),)));

        }

      }));


    }
  }

  final _formkey = GlobalKey<FormState>();
  DateTime? currentBackPressTime;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Form(
            key: _formkey,
            child: Scaffold(
              body: SingleChildScrollView(
                  child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    color: Colors.transparent,
                    child: Ink.image(
                      image: AssetImage("images/acc1.jpg"),
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.height,
                      height: MediaQuery.of(context).size.width,
                      child: InkWell(
                        onTap: () {},
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.height / 2,
                      ),
                      Container(
                          height: MediaQuery.of(context).size.height / 2,
                          width: MediaQuery.of(context).size.width,
                          decoration: const BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                  topRight: Radius.circular(50),
                                  topLeft: Radius.circular(50))),
                          child: SingleChildScrollView(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                SizedBox(
                                  height: 50,
                                ),
                                Container(
                                  padding: const EdgeInsets.all(5),
                                  height: 65,
                                  width:
                                      MediaQuery.of(context).size.width / 1.25,
                                  decoration: BoxDecoration(
                                    color: Colors.orange.shade50,
                                    boxShadow: const [
                                      BoxShadow(
                                          offset: Offset(0, 0),
                                          blurRadius: 0,
                                          color: Colors.transparent)
                                    ],
                                  ),
                                  child: Center(
                                    child: TextFormField(
                                      controller: userName,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return AppLocalizations.of(context)!.userNameRequire;
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        hintStyle: TextStyle(fontSize: 14),
                                        hintText: AppLocalizations.of(context)!.user,
                                        suffixIcon: Icon(
                                          Icons.account_circle_rounded,
                                          color: Colors.black45,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Container(
                                  padding: const EdgeInsets.all(5),
                                  height: 65,
                                  width:
                                      MediaQuery.of(context).size.width / 1.25,
                                  decoration: BoxDecoration(
                                    color: Colors.orange.shade50,
                                    boxShadow: const [
                                      BoxShadow(
                                          offset: Offset(0, 0),
                                          blurRadius: 0,
                                          color: Colors.transparent)
                                    ],
                                  ),
                                  child: Center(
                                    child: TextFormField(
                                      controller: psw,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return AppLocalizations.of(context)!.passwordRequire;
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        border: UnderlineInputBorder(),
                                        hintStyle: TextStyle(fontSize: 14),
                                        hintText: AppLocalizations.of(context)!.password,
                                        suffixIcon: Icon(
                                          Icons.lock_open_rounded,
                                          color: Colors.black45,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 60,
                                ),
                                buildButton(AppLocalizations.of(context)!.loginButton, Colors.white,
                                    CupertinoColors.systemBlue, connection),
                                SizedBox(
                                  height: 15,
                                ),
                                RichText(
                                  text: TextSpan(
                                      text: AppLocalizations.of(context)!.ifNoAccount,
                                      style: const TextStyle(
                                          color: Colors.black, fontSize: 14),
                                      children: [
                                        TextSpan(
                                          text: AppLocalizations.of(context)!.registerLink,
                                          style: TextStyle(
                                              color: Colors.orange,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 20),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.push(context,
                                                  MaterialPageRoute(builder:
                                                      (BuildContext context) {
                                                return RegisterPage(
                                                    title: "title");
                                              }));
                                            },
                                        ),
                                      ]),
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                              ],
                            ),
                          )),
                    ],
                  ),
                ],
              )),
            )),
        onWillPop: () {
          DateTime now = DateTime.now();
          if (currentBackPressTime == null ||
              now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
            currentBackPressTime = now;
            ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                backgroundColor: Colors.transparent,
                content: Text(
                  "Tap again to Exit",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black),
                )));
            return Future.value(false);
          } else {
            SystemNavigator.pop();
            return Future.value(false);}
        });
  }


}
