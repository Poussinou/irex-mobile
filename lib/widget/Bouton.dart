import 'package:flutter/material.dart';



Widget buildButton(String text, Color tcolor, Color bcolor, VoidCallback onPressed) {
  return Container(
    width: 312,
    height: 57,
    child: ElevatedButton(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(bcolor),
          minimumSize: MaterialStateProperty.all(Size(287, 57)),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12)
          ))
      ),
      onPressed: onPressed,
      child: Text(text, style: TextStyle(color: tcolor,fontSize: 36)),
    ),
  );
}
