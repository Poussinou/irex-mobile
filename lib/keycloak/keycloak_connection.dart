
import 'dart:convert';

import 'env.dart';
//import 'package:flutter_appauth/flutter_appauth.dart';

import 'package:http/http.dart' as http;

class KeycloakConnection{
  Future<Object> login(String usrname, String password) async {
    String url = Environement.SSO_URL + '/protocol/openid-connect/token';
    final uri = Uri.parse(url);
    Map<String, String> headers = {
      'content-type' : 'application/x-www-form-urlencoded',
      'cache-control' : 'no-cache',
    };
    Map<String, String> data = {
      'client_id' : Environement.CLIENT_ID,
      'client_secret' : 'ho5vrnF5wd8UZGyyqDO3NDUKx0uOLqs2',
      'username' : usrname,
      'password' : password,
      'grant_type' : 'password'
    };

    http.Response response = await http.post(uri , headers: headers, body: data);
    print(response.body);
    print(response.statusCode);

    if(response.statusCode == 200){
      dynamic token = json.decode(response.body)['access_token'].toString();
      return "AuthenticationResponse.AUTHENTICATED";
    }else if(response.statusCode == 401){
      return throw("NOT_AUTHORIZED");
    }else if(response.statusCode == 400){
      return throw("NEED_EMAIL_VERIFICATION");
    }else{
      throw("Server Error");
    }
  }
}
